package ru.t1.aksenova.tm.command.user;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.aksenova.tm.api.endpoint.IUserEndpoint;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.dto.model.UserDTO;

@Getter
@Setter
@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpointClient;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) return;
        System.out.println("USER ID: " + user.getId());
        System.out.println("USER LOGIN: " + user.getLogin());
        System.out.println("USER E-MAIL: " + user.getEmail());
        System.out.println("USER ROLE: " + user.getRole().getDisplayName());
    }

}
