package ru.t1.aksenova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.dto.request.UserUnlockRequest;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.util.TerminalUtil;

@Component
public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-unlock";

    @NotNull
    public static final String DESCRIPTION = "User unlock.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();

        @Nullable final UserUnlockRequest request = new UserUnlockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().unlockUser(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
