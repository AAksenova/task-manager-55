package ru.t1.aksenova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.dto.request.UserLoginRequest;
import ru.t1.aksenova.tm.dto.response.UserLoginResponse;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.util.TerminalUtil;

@Component
public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "login";

    @NotNull
    public static final String DESCRIPTION = "User login.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();

        @Nullable final UserLoginRequest request = new UserLoginRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        @Nullable final UserLoginResponse response = getAuthEndpointClient().login(request);
        @Nullable final String token = response.getToken();
        setToken(token);
        System.out.println(token);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
