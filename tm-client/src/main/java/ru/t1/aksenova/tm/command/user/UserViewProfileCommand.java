package ru.t1.aksenova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.dto.request.UserViewProfileRequest;
import ru.t1.aksenova.tm.dto.response.UserViewProfileResponse;
import ru.t1.aksenova.tm.enumerated.Role;

@Component
public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "view-user-profile";

    @NotNull
    public static final String DESCRIPTION = "View profile of current user.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final UserViewProfileRequest request = new UserViewProfileRequest(getToken());
        @Nullable final UserViewProfileResponse response = getAuthEndpointClient().viewProfile(request);
        @Nullable final UserDTO user = response.getUser();
        System.out.println("USER ID: " + user.getId());
        System.out.println("USER LOGIN: " + user.getLogin());
        System.out.println("USER E-MAIL: " + user.getEmail());
        System.out.println("USER FIRST NAME: " + user.getFirstName());
        System.out.println("USER LAST NAME: " + user.getLastName());
        System.out.println("USER MIDDLE NAME: " + user.getMiddleName());
        System.out.println("USER ROLE: " + user.getRole().getDisplayName());
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
