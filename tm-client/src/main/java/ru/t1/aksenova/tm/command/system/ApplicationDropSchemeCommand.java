package ru.t1.aksenova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.dto.request.DropSchemeRequest;
import ru.t1.aksenova.tm.dto.response.DropSchemeResponse;

@Component
public final class ApplicationDropSchemeCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "scheme-drop";

    @NotNull
    public static final String DESCRIPTION = "Drop database scheme.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[DROP DATABASE SCHEME]");
        @NotNull final String token = "cuefn+Az4fVqckVBzc/OKcJmnPZa93HDkaqZFGQVDZXK4SEmBvsdcgXiIQQRLb7B1zhP6GX5e8BulR7I+q+CkcN5hvT+yoqBLgJ3iQ9xVVNda+oPfnqSuC1nKrFxZmpk3uaAb5vc/0VuuNsYaAuCnar5QxFMC9aOyVXyxZORgdO1mLaK+6Xhn7XThG0+V4Q5";
        @NotNull final DropSchemeRequest request = new DropSchemeRequest(token);
        @NotNull final DropSchemeResponse response = getAdminEndpoint().dropDBScheme(request);
        System.out.println("RESULT: " + response.getResult());
    }

}
