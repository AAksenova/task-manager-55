package ru.t1.aksenova.tm.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.api.service.dto.*;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.exception.AbstractException;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.migration.AbstractSchemeTest;
import ru.t1.aksenova.tm.service.dto.*;

import java.util.List;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.USER_TEST;

@Category(UnitCategory.class)
@Ignore
public final class ProjectTaskServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);
    @NotNull
    private static final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(projectService, taskService);
    @NotNull
    private static final ISessionDTOService sessionService = new SessionDTOService(connectionService);
    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService, sessionService);

    @NotNull
    private static String userId = "";

    public static void initConnectionService() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    public static void clearData() {
        @Nullable UserDTO user = userService.findOneById(userId);
        if (user != null) userService.remove(user);
    }

    @BeforeClass
    public static void initData() throws Exception {
        initConnectionService();
        @NotNull final UserDTO user = userService.add(USER_TEST);
        userId = user.getId();
        USER_PROJECT1.setUserId(userId);
        USER_PROJECT2.setUserId(userId);
        USER_TASK1.setUserId(userId);
        USER_TASK2.setUserId(userId);
        USER_TASK3.setUserId(userId);
    }

    @AfterClass
    public static void destroy() {
        clearData();
        connectionService.close();
    }

    @Before
    public void beforeTest() {
        projectService.add(USER_PROJECT1);
        projectService.add(USER_PROJECT2);
        taskService.add(USER_TASK1);
        taskService.add(USER_TASK2);
        taskService.add(USER_TASK3);
    }

    @After
    public void afterTest() {
        taskService.removeAll(userId);
        projectService.removeAll(userId);
    }

    @Test
    public void bindTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(null, USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject("", USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, null, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, "", USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, USER_PROJECT2.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, USER_PROJECT2.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.bindTaskToProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK3.getId()));
        projectTaskService.bindTaskToProject(userId, USER_PROJECT2.getId(), USER_TASK3.getId());
        @Nullable final TaskDTO task = taskService.findOneById(userId, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_PROJECT2.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(null, USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject("", USER_PROJECT2.getId(), USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, null, USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, "", USER_TASK3.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, USER_PROJECT2.getId(), null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, USER_PROJECT2.getId(), ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.unbindTaskToProject(userId, NON_EXISTING_PROJECT_ID, USER_TASK3.getId()));
        projectTaskService.unbindTaskToProject(userId, USER_PROJECT2.getId(), USER_TASK3.getId());
        @Nullable final TaskDTO task = taskService.findOneById(userId, USER_TASK3.getId());
        Assert.assertNotNull(task);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void removeTaskToProject() {
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(null, USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject("", USER_PROJECT1.getId()));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(userId, null));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(userId, ""));
        Assert.assertThrows(AbstractException.class, () -> projectTaskService.removeTaskToProject(userId, NON_EXISTING_PROJECT_ID));

        @NotNull List<TaskDTO> tasks = taskService.findAllByProjectId(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(tasks);
        projectTaskService.removeTaskToProject(userId, USER_PROJECT1.getId());
    }

}
