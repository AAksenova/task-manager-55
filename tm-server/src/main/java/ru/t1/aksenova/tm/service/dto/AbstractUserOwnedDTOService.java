package ru.t1.aksenova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.aksenova.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDTORepository<M>>
        extends AbstractDTOService<M, R> implements IUserOwnedDTOService<M> {

    public AbstractUserOwnedDTOService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @Nullable
    @Override
    public M add(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnedDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void update(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnedDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull String userId, @NotNull M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserOwnedDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(userId, model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
