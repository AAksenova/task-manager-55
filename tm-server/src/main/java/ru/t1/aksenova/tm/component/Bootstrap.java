package ru.t1.aksenova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.endpoint.*;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.api.service.dto.*;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.endpoint.*;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.service.*;
import ru.t1.aksenova.tm.service.dto.*;
import ru.t1.aksenova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @Getter
    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(projectService, taskService);

    @Getter
    @NotNull
    private final ISessionDTOService sessionService = new SessionDTOService(connectionService);

    @Getter
    @NotNull
    private final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService, sessionService);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);
    @Getter
    @NotNull
    private final IAdminService adminService = new AdminService(propertyService, connectionService);
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(connectionService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(adminEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() {
        initDemoData();
        loggerService.initJmsLogger();
        loggerService.info("** WELCOME TO SERVER TASK-MANAGER **");
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    private void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        try {
            if (!userService.isLoginExist("test")) {
                @NotNull final UserDTO test = userService.create("test", "test", "test@test.ru");
                projectService.add(test.getId(), new ProjectDTO("One test project", Status.IN_PROGRESS));
                projectService.add(test.getId(), new ProjectDTO("Two test project", Status.NOT_STARTED));
                projectService.add(test.getId(), new ProjectDTO("Three test project", Status.COMPLETED));
                taskService.add(test.getId(), new TaskDTO("Alfa task", Status.IN_PROGRESS));
                taskService.add(test.getId(), new TaskDTO("Beta task", Status.COMPLETED));
                taskService.add(test.getId(), new TaskDTO("Gamma task", Status.NOT_STARTED));
            }
            if (!userService.isLoginExist("user")) {
                @NotNull final UserDTO user = userService.create("user", "user", "user@user.ru");
                projectService.add(user.getId(), new ProjectDTO("Six test project", Status.NOT_STARTED));
                taskService.add(user.getId(), new TaskDTO("Teuta task", Status.IN_PROGRESS));
            }
            if (!userService.isLoginExist("admin")) {
                @NotNull final UserDTO admin = userService.create("admin", "admin", Role.ADMIN);
                projectService.add(admin.getId(), new ProjectDTO("Four test project", Status.NOT_STARTED));
                projectService.add(admin.getId(), new ProjectDTO("Five test project", Status.COMPLETED));
                taskService.add(admin.getId(), new TaskDTO("Delta task", Status.IN_PROGRESS));
                taskService.add(admin.getId(), new TaskDTO("Eta task", Status.NOT_STARTED));
            }
        } finally {

        }
    }

}
