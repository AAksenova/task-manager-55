package ru.t1.aksenova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.aksenova.tm.comparator.CreatedComparator;
import ru.t1.aksenova.tm.comparator.NameComparator;
import ru.t1.aksenova.tm.comparator.StatusComparator;
import ru.t1.aksenova.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;

public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTORepository<M> implements IUserOwnedDTORepository<M> {

    public AbstractUserOwnedDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == NameComparator.INSTANCE) return "name";
        else if (comparator == CreatedComparator.INSTANCE) return "created";
        else return "created";
    }

    @Nullable
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty() || userId == null) return null;
        model.setUserId(userId);
        entityManager.persist(model);
        return model;
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty() || userId == null) return;
        model.setUserId(userId);
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty() || userId == null) return;
        model.setUserId(userId);
        entityManager.remove(model);
    }

}
