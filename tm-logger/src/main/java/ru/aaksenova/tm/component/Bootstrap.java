package ru.aaksenova.tm.component;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.aaksenova.tm.listener.EntityListener;
import ru.aaksenova.tm.service.LoggerService;

import javax.jms.*;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @SneakyThrows
    public void start() {
        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        final Queue destination = session.createQueue(QUEUE);
        final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }


}
